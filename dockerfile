FROM ubuntu:14.04

# Update the default application repository sources list
RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y python python-pip
RUN apt-get install -y python-dev
RUN apt-get install -y libmysqlclient-dev
RUN apt-get install -y git
RUN apt-get install -y sqlite3
RUN apt-get install -y vim
RUN apt-get install -y mysql-server
RUN apt-get install -y nginx
COPY . /src
RUN pip install -r /src/requirement.txt
EXPOSE 8001
# Copy entrypoint script into the image
WORKDIR /src/imdb
CMD gunicorn -b '0.0.0.0:8001' --timeout 180 --worker-class gevent -w 8 shipmentservice.wsgi:application
