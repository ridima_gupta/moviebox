from elasticsearch import Elasticsearch

ES_HOST = {"host": "localhost",
           "port": 9201}

es = Elasticsearch(hosts=[ES_HOST])

INDEX_NAME = 'imdb-index'


def build_query_with_imdb_score(imdb_score_lte, imdb_score_gte):
    """
    Query when user filters data using the imdb score
    :param imdb_score_lte:
    :param imdb_score_gte:
    :return:
    """
    query_with_imdb_score = {
        "query": {
            "range": {
                "imdb_score": {
                    "gte": imdb_score_gte,
                    "lte": imdb_score_lte
                }
            }
        }
    }
    return query_with_imdb_score


def build_query_with_popularity(popularity_lte, popularity_gte):
    """
    Query when the user filters data using popularity
    :param popularity_lte:
    :param popularity_gte:
    :return:
    """
    query_with_popularity = {
        "query": {
            "range": {
                "popularity": {
                    "gte": popularity_gte,
                    "lte": popularity_lte
                }
            }
        }
    }
    return query_with_popularity


def build_query_with_genre(genre_term):
    """
    Query when user filters data using the genre
    :param genre_term:
    :return:
    """
    query_with_genre = {
        "query": {
            "match": {
                "genre": genre_term
            }
        }
    }
    return query_with_genre


def build_query_with_director_name(director_name):
    """
    Query when user filters data using the director name
    :param director_name:
    :return:
    """
    query_with_director_name = {
        "query": {
            "match_phrase": {
                "director_name": director_name
            }
        }
    }
    return query_with_director_name


def build_query_for_searchterm(searchterm):
    """
    Query when user filters data using the search term i.e. movie name.
    :param searchterm:
    :return:
    """
    query_with_search_term_movie_name = {
        "query": {
            "match_phrase": {
                "name": searchterm
            }
        }
    }
    return query_with_search_term_movie_name


def build_query_for_all(searchterm, genre, imdb_score_lte,
                        imdb_score_gte, popularity_lte, popularity_gte,
                        director_name):
    """
    Query when user filters data using the movie name, genre,
    imdb score, popularity and director name.
    :param searchterm:
    :param genre:
    :param imdb_score_lte:
    :param imdb_score_gte:
    :param popularity_lte:
    :param popularity_gte:
    :param director_name:
    :return:
    """
    query_with_all = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "name": searchterm
                            }
                        },
                        {
                            "match": {
                                "genre": genre
                            }
                        },
                        {
                            "range": {
                                "imdb_score": {
                                    "gte": imdb_score_gte,
                                    "lte": imdb_score_lte
                                }
                            }
                        },
                        {
                            "range": {
                                "popularity": {
                                    "gte": popularity_gte,
                                    "lte": popularity_lte
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "director_name": director_name
                            }
                        }
                    ]
                }
            }
        }
    return query_with_all


def build_query_for_searchterm_director(searchterm, director_name):
    """
    Query when user filters data using the director name and movie name
    :param searchterm:
    :param director_name:
    :return:
    """
    query_for_searchterm_director = {
        "query": {
            "bool": {
                "must": [
                    {
                        "match_phrase": {
                            "name": searchterm
                        }
                    },
                    {
                        "match_phrase": {
                            "director_name": director_name
                        }
                    }
                ]
            }
        }

    }
    return query_for_searchterm_director


def build_query_for_searchterm_imdb_score(searchterm,
                                          imdb_score_lte,
                                          imdb_score_gte):
    """
    Query when user filters data using the search term
    i.e. movie name and imdb score
    :param searchterm:
    :param imdb_score_lte:
    :param imdb_score_gte:
    :return:
    """
    query_for_searchterm_imdb_score = {
        "query": {
            "bool": {
                "must": [
                    {
                        "match_phrase": {
                            "name": searchterm
                        }
                    },
                    {
                        "range": {
                            "imdb_score": {
                                "gte": imdb_score_gte,
                                "lte": imdb_score_lte
                            }
                        }
                    }
                ]
            }
        }

    }
    return query_for_searchterm_imdb_score


def build_query_for_searchterm_popularity(searchterm,
                                          popularity_lte,
                                          popularity_gte):
    """
    Query when user filters data using the search term
    i.e. movie name and popularity
    :param searchterm:
    :param popularity_lte:
    :param popularity_gte:
    :return:
    """
    query_for_searchterm_popularity = {
        "query": {
            "bool": {
                "must": [
                    {
                        "match_phrase": {
                            "name": searchterm
                        }
                    },
                    {
                        "range": {
                            "popularity": {
                                "gte": popularity_gte,
                                "lte": popularity_lte
                            }
                        }
                    }
                ]
            }
        }

    }
    return query_for_searchterm_popularity


def build_query_for_searchterm_genre(searchterm, genre):
    """
    Query when user filters data using the search term
    i.e. movie name and genre
    :param searchterm:
    :param genre:
    :return:
    """
    query_for_searchterm_genre = {
        "query": {
            "bool": {
                "must": [
                    {
                        "match_phrase": {
                            "name": searchterm
                        }
                    },
                    {
                        "match": {
                            "genre": genre
                        }
                    }
                ]
            }
        }
    }
    return query_for_searchterm_genre


def build_query_for_director_name_imdb_score(imdb_score_lte,
                                             imdb_score_gte,
                                             director_name):
    """
    Query when user filters data using the director name and imdb score
    :param imdb_score_lte:
    :param imdb_score_gte:
    :param director_name:
    :return:
    """
    query_for_director_name_imdb_score = {
        "query": {
            "bool": {
                "must": [
                    {
                        "range": {
                            "imdb_score": {
                                "gte": imdb_score_gte,
                                "lte": imdb_score_lte
                            }
                        }
                    },
                    {
                        "match_phrase": {
                            "director_name": director_name
                        }
                    }
                ]
            }
        }

    }
    return query_for_director_name_imdb_score


def build_query_for_director_name_popularity(popularity_lte,
                                             popularity_gte,
                                             director_name):
    """
    Query when user filters data using director name and popularity
    :param popularity_lte:
    :param popularity_gte:
    :param director_name:
    :return:
    """
    query_for_director_name_popularity = {
        "query": {
            "bool": {
                "must": [
                    {
                        "range": {
                            "popularity": {
                                "gte": popularity_gte,
                                "lte": popularity_lte
                            }
                        }
                    },
                    {
                        "match_phrase": {
                            "director_name": director_name
                        }
                    }
                ]
            }
        }

    }
    return query_for_director_name_popularity


def build_query_for_director_name_genre(genre, director_name):
    """
    Query when user filters data using the director name and genre
    :param genre:
    :param director_name:
    :return:
    """
    query_for_director_name_genre = {
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "genre": genre
                        }
                    },
                    {
                        "match_phrase": {
                            "director_name": director_name
                        }
                    }
                ]
            }
        }

    }
    return query_for_director_name_genre


def build_query_for_imdb_score_popularity(imdb_score_lte,
                                          imdb_score_gte,
                                          popularity_lte,
                                          popularity_gte):
    """
    Query when user filters data using the imdb score and popularity
    :param imdb_score_lte:
    :param imdb_score_gte:
    :param popularity_lte:
    :param popularity_gte:
    :return:
    """
    query_for_imdb_score_popularity = {
        "query": {
            "bool": {
                "must": [
                    {
                        "range": {
                            "imdb_score": {
                                "gte": imdb_score_gte,
                                "lte": imdb_score_lte
                            }
                        }
                    },
                    {
                        "range": {
                            "popularity": {
                                "gte": popularity_gte,
                                "lte": popularity_lte
                            }
                        }
                    }
                ]
            }
        }

    }
    return query_for_imdb_score_popularity


def build_query_for_imdb_score_genre(genre, imdb_score_lte, imdb_score_gte):
    """
    Query when user filters data using imdb score and genre.
    :param genre:
    :param imdb_score_lte:
    :param imdb_score_gte:
    :return:
    """
    query_for_imdb_score_genre = {
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "genre": genre
                        }
                    },
                    {
                        "range": {
                            "imdb_score": {
                                "gte": imdb_score_gte,
                                "lte": imdb_score_lte
                            }
                        }
                    }
                ]
            }
        }

    }
    return query_for_imdb_score_genre


def build_query_for_popularity_genre(genre, popularity_lte, popularity_gte):
    """
    Query when user filters data using popularity and genre.
    :param genre:
    :param popularity_lte:
    :param popularity_gte:
    :return:
    """
    query_for_popularity_genre = {
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "genre": genre
                        }
                    },
                    {
                        "range": {
                            "popularity": {
                                "gte": popularity_gte,
                                "lte": popularity_lte
                            }
                        }
                    }
                ]
            }
        }

    }
    return query_for_popularity_genre


def build_query_for_searchterm_director_name_imdb_range(
        searchterm, director_name, imdb_score_lte, imdb_score_gte):
    """
    Query when user filters data using the search term
    i.e. movie name, director name and imdb score.
    :param searchterm:
    :param director_name:
    :param imdb_score_lte:
    :param imdb_score_gte:
    :return:
    """
    query_for_searchterm_director_name_imdb_range = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "name": searchterm
                            }
                        },
                        {
                            "range": {
                                "imdb_score": {
                                    "gte": imdb_score_gte,
                                    "lte": imdb_score_lte
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "director_name": director_name
                            }
                        }
                    ]
                }
            }
        }
    return query_for_searchterm_director_name_imdb_range


def build_query_for_searchterm_imdb_range_popularity_range(
        searchterm, imdb_score_gte, imdb_score_lte,
        popularity_gte,
        popularity_lte):
    """
    Query when user filters data using the search term
    i.e. movie name, imdb score and popularity
    :param searchterm:
    :param imdb_score_gte:
    :param imdb_score_lte:
    :param popularity_gte:
    :param popularity_lte:
    :return:
    """
    query_for_searchterm_imdb_range_popularity_range = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "name": searchterm
                            }
                        },
                        {
                            "range": {
                                "imdb_score": {
                                    "gte": imdb_score_gte,
                                    "lte": imdb_score_lte
                                }
                            }
                        },
                        {
                            "range": {
                                "popularity": {
                                    "gte": popularity_gte,
                                    "lte": popularity_lte
                                }
                            }
                        }
                    ]
                }
            }

        }
    return query_for_searchterm_imdb_range_popularity_range


def build_query_for_searchterm_popularity_range_genre(
        searchterm, popularity_gte, popularity_lte, genre):
    """
    Query when user filters data using the search term
    i.e. movie name, popularity and gemre.
    :param searchterm:
    :param popularity_gte:
    :param popularity_lte:
    :param genre:
    :return:
    """
    query_for_searchterm_popularity_range_genre = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "name": searchterm
                            }
                        },
                        {
                            "match": {
                                "genre": genre
                            }
                        },
                        {
                            "range": {
                                "popularity": {
                                    "gte": popularity_gte,
                                    "lte": popularity_lte
                                }
                            }
                        }
                    ]
                }
            }
        }
    return query_for_searchterm_popularity_range_genre


def build_query_for_searchterm_director_name_genre(
        searchterm, director_name, genre):
    """
    Query when user filters data using the search term
    i.e. movie name, director name and genre.
    :param searchterm:
    :param director_name:
    :param genre:
    :return:
    """
    query_for_searchterm_director_name_genre = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "name": searchterm
                            }
                        },
                        {
                            "match": {
                                "genre": genre
                            }
                        },
                        {
                            "match_phrase": {
                                "director_name": director_name
                            }
                        }
                    ]
                }
            }

        }
    return query_for_searchterm_director_name_genre


def build_query_for_director_name_popularity_range_imdb_score_range(
        director_name, popularity_lte, popularity_gte,
        imdb_score_gte, imdb_score_lte):
    """
    Query when user filters data using
    the director name, popularity and imdb score.
    :param director_name:
    :param popularity_lte:
    :param popularity_gte:
    :param imdb_score_gte:
    :param imdb_score_lte:
    :return:
    """
    query_for_director_name_popularity_range_imdb_score_range = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "range": {
                                "imdb_score": {
                                    "gte": imdb_score_gte,
                                    "lte": imdb_score_lte
                                }
                            }
                        },
                        {
                            "range": {
                                "popularity": {
                                    "gte": popularity_gte,
                                    "lte": popularity_lte
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "director_name": director_name
                            }
                        }
                    ]
                }
            }

        }
    return query_for_director_name_popularity_range_imdb_score_range


def build_query_for_director_name_popularity_range_genre(
        director_name, popularity_lte, popularity_gte, genre):
    """
    Query when user filters data using
    the director name, popularity and genre.
    :param director_name:
    :param popularity_lte:
    :param popularity_gte:
    :param genre:
    :return:
    """
    query_for_director_name_popularity_range_genre = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                "genre": genre
                            }
                        },
                        {
                            "range": {
                                "popularity": {
                                    "gte": popularity_gte,
                                    "lte": popularity_lte
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "director_name": director_name
                            }
                        }
                    ]
                }
            }
        }
    return query_for_director_name_popularity_range_genre


def build_query_for_imdb_score_popularity_genre(
        genre, imdb_score_gte, imdb_score_lte,
        popularity_gte, popularity_lte):
    """
    Query when user filters data using the imdb score, popularity and genre.
    :param genre:
    :param imdb_score_gte:
    :param imdb_score_lte:
    :param popularity_gte:
    :param popularity_lte:
    :return:
    """
    query_for_imdb_score_popularity_genre = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                "genre": genre
                            }
                        },
                        {
                            "range": {
                                "imdb_score": {
                                    "gte": imdb_score_gte,
                                    "lte": imdb_score_lte
                                }
                            }
                        },
                        {
                            "range": {
                                "popularity": {
                                    "gte": popularity_gte,
                                    "lte": popularity_lte
                                }
                            }
                        }
                    ]
                }
            }

        }
    return query_for_imdb_score_popularity_genre


def build_query_for_searchterm_imdb_score_genre(
        searchterm, imdb_score_lte, imdb_score_gte, genre):
    """
    Query when user filters data using the search term
    i.e. movie name, imdb score and genre.
    :param searchterm:
    :param imdb_score_lte:
    :param imdb_score_gte:
    :param genre:
    :return:
    """
    query_for_searchterm_imdb_score_genre = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "name": searchterm
                            }
                        },
                        {
                            "match": {
                                "genre": genre
                            }
                        },
                        {
                            "range": {
                                "imdb_score": {
                                    "gte": imdb_score_gte,
                                    "lte": imdb_score_lte
                                }
                            }
                        }
                    ]
                }
            }
        }
    return query_for_searchterm_imdb_score_genre


def build_query_for_searchterm_popularity_range_genre(
        searchterm, popularity_gte, popularity_lte, genre):
    """
    Query when user filters data using the search term
    i.e. movie name, popularity and genre
    :param searchterm:
    :param popularity_gte:
    :param popularity_lte:
    :param genre:
    :return:
    """
    query_for_searchterm_popularity_range_genre = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "name": searchterm
                            }
                        },
                        {
                            "match": {
                                "genre": genre
                            }
                        },
                        {
                            "range": {
                                "popularity": {
                                    "gte": popularity_gte,
                                    "lte": popularity_lte
                                }
                            }
                        }
                    ]
                }
            }
        }
    return query_for_searchterm_popularity_range_genre


def build_query_for_searchterm_director_name_popularity(
        searchterm, director_name, popularity_lte, popularity_gte):
    """
    Query when user filters data using the search term
    i.e. movie name, director name and popularity
    :param searchterm:
    :param director_name:
    :param popularity_lte:
    :param popularity_gte:
    :return:
    """
    query_for_searchterm_director_name_popularity = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "name": searchterm
                            }
                        },
                        {
                            "range": {
                                "popularity": {
                                    "gte": popularity_gte,
                                    "lte": popularity_lte
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "director_name": director_name
                            }
                        }
                    ]
                }
            }
        }
    return query_for_searchterm_director_name_popularity


def build_query_for_director_name_imdb_score_range_genre(
        director_name, genre, imdb_score_gte, imdb_score_lte):
    """
    Query when user filters data using
    director name, imdb score and genre.
    :param director_name:
    :param genre:
    :param imdb_score_gte:
    :param imdb_score_lte:
    :return:
    """
    query_for_director_name_imdb_score_range_genre = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                "genre": genre
                            }
                        },
                        {
                            "range": {
                                "imdb_score": {
                                    "gte": imdb_score_gte,
                                    "lte": imdb_score_lte
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "director_name": director_name
                            }
                        }
                    ]
                }
            }
        }
    return query_for_director_name_imdb_score_range_genre


def build_query_for_searchterm_director_name_imdb_score_popularity_range(
        searchterm, director_name, imdb_score_gte,
        imdb_score_lte, popularity_lte, popularity_gte):
    """
    Query when user filters data using the search term i.e.
    movie name, director name, imdb score and popularity.
    :param searchterm:
    :param director_name:
    :param imdb_score_gte:
    :param imdb_score_lte:
    :param popularity_lte:
    :param popularity_gte:
    :return:
    """
    query_for_searchterm_director_name_imdb_score_popularity_range = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "name": searchterm
                            }
                        },
                        {
                            "range": {
                                "imdb_score": {
                                    "gte": imdb_score_gte,
                                    "lte": imdb_score_lte
                                }
                            }
                        },
                        {
                            "range": {
                                "popularity": {
                                    "gte": popularity_gte,
                                    "lte": popularity_lte
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "director_name": director_name
                            }
                        }
                    ]
                }
            }
        }
    return query_for_searchterm_director_name_imdb_score_popularity_range


def build_query_for_searchterm_imdb_score_popularity_range_genre(
        searchterm, imdb_score_gte, imdb_score_lte, popularity_gte,
        popularity_lte, genre):
    """
    Query when user filters data using the search term
    i.e. movie name, imdb score, popularity and genre.
    :param searchterm:
    :param imdb_score_gte:
    :param imdb_score_lte:
    :param popularity_gte:
    :param popularity_lte:
    :param genre:
    :return:
    """
    query_for_searchterm_imdb_score_popularity_range_genre = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "name": searchterm
                            }
                        },
                        {
                            "match": {
                                "genre": genre
                            }
                        },
                        {
                            "range": {
                                "imdb_score": {
                                    "gte": imdb_score_gte,
                                    "lte": imdb_score_lte
                                }
                            }
                        },
                        {
                            "range": {
                                "popularity": {
                                    "gte": popularity_gte,
                                    "lte": popularity_lte
                                }
                            }
                        }
                    ]
                }
            }
        }
    return query_for_searchterm_imdb_score_popularity_range_genre


def build_query_for_searchterm_director_name_popularity_range_genre(
        searchterm, director_name, popularity_gte, popularity_lte, genre):
    """
    Query when user filters data using the search term
    i.e. movie name, director name, popularity and genre.
    :param searchterm:
    :param director_name:
    :param popularity_gte:
    :param popularity_lte:
    :param genre:
    :return:
    """
    query_for_searchterm_director_name_popularity_range_genre = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "name": searchterm
                            }
                        },
                        {
                            "match": {
                                "genre": genre
                            }
                        },
                        {
                            "range": {
                                "popularity": {
                                    "gte": popularity_gte,
                                    "lte": popularity_lte
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "director_name": director_name
                            }
                        }
                    ]
                }
            }
        }
    return query_for_searchterm_director_name_popularity_range_genre


def build_query_for_searchterm_director_name_imdb_score_genre(
        searchterm, director_name,
        imdb_score_gte, imdb_score_lte, genre):
    """
    Query when user filters data using the search term
    i.e. movie name, director name, imdb core and genre.
    :param searchterm:
    :param director_name:
    :param imdb_score_gte:
    :param imdb_score_lte:
    :param genre:
    :return:
    """
    query_for_searchterm_director_name_imdb_score_genre = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match_phrase": {
                                "name": searchterm
                            }
                        },
                        {
                            "match": {
                                "genre": genre
                            }
                        },
                        {
                            "range": {
                                "imdb_score": {
                                    "gte": imdb_score_gte,
                                    "lte": imdb_score_lte
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "director_name": director_name
                            }
                        }
                    ]
                }
            }

        }
    return query_for_searchterm_director_name_imdb_score_genre


def build_query_for_director_name_imdb_score_popularity_range_genre(
        director_name, imdb_score_gte, imdb_score_lte,
        popularity_gte, popularity_lte, genre):
    """
    Query when user filters data using the
    director name, imdb score, popularity and genre.
    :param director_name:
    :param imdb_score_gte:
    :param imdb_score_lte:
    :param popularity_gte:
    :param popularity_lte:
    :param genre:
    :return:
    """
    query_for_director_name_imdb_score_popularity_range_genre = \
        {
            "query": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                "genre": genre
                            }
                        },
                        {
                            "range": {
                                "imdb_score": {
                                    "gte": imdb_score_gte,
                                    "lte": imdb_score_lte
                                }
                            }
                        },
                        {
                            "range": {
                                "popularity": {
                                    "gte": popularity_gte,
                                    "lte": popularity_lte
                                }
                            }
                        },
                        {
                            "match_phrase": {
                                "director_name": director_name
                            }
                        }
                    ]
                }
            }

        }
    return query_for_director_name_imdb_score_popularity_range_genre


def build_query_for_fetching_recommendation(genre):
    """
    Query to fetch recommendated movies for a user
    based on the genre that he has viewed.
    :param genre:
    :return:
    """
    query_for_fetching_recommendations = \
        {
            "from": 0,
            "size": 10,
            "query": {
                "bool": {
                    "must": [
                        {
                            "match": {
                                "genre": genre
                            }
                        }
                    ]
                }
            },
            "sort": [
                {
                    "imdb_score": {
                        "order": "desc"
                    }
                }
            ]

        }
    return query_for_fetching_recommendations


def fetch_recommendation(genre):
    query = build_query_for_fetching_recommendation(genre)
    return es.search(index=INDEX_NAME, body=query)


# searchterm = "Star Wars"
# director_name = "Marc Daniels"
# genre_term = "Adventure, Sci-Fi"
# imdb_score = 4:9
# popularity = 34:89

def search(searchterm, director_name, genre_term, imdb_score_range,
           popularity_range):
    """
    Format in which we expect data is:
        searchterm = "Star Wars"
        director_name = "Marc Daniels"
        genre_term = "Adventure, Sci-Fi"
        imdb_score = 4:9
        popularity = 34:89
    :param searchterm: the movie name that user would have put
    :param director_name: director's name selected from filter
    :param genre_term: Genre selected from filter
    :param imdb_score_range:
    Imdb score range selected from filter 1 to 10
    :param popularity_range:
    Popularity range selected from filter 1 to 100
    :return: Returns data from elastic-search.
    """

    if imdb_score_range and ":" in imdb_score_range:
        imdb_data = imdb_score_range.split(':')
        imdb_score_gte = float(imdb_data[0])
        imdb_score_lte = float(imdb_data[1])
    elif imdb_score_range:
        # imdb_data = imdb_score_range.split(':')
        imdb_score_gte = float(imdb_score_range)
        imdb_score_lte = float(imdb_score_range)

    if popularity_range and ":" in popularity_range:
        popularity_data = popularity_range.split(':')
        popularity_gte = float(popularity_data[0])
        popularity_lte = float(popularity_data[1])
    elif popularity_range:
        # popularity_data = popularity_range.split(':')
        popularity_gte = float(popularity_range)
        popularity_lte = float(popularity_range)

    res = None
    error = None

    if not searchterm and not director_name and not genre_term \
            and not imdb_score_range and not popularity_range:
        error = "Please give a valid input."
        return error, res["hits"]["hits"]

    elif searchterm and not director_name and not genre_term \
            and not imdb_score_range and not popularity_range:
        query = build_query_for_searchterm(searchterm)
        res = es.search(index=INDEX_NAME, body=query)

    elif director_name and not searchterm and not genre_term \
            and not imdb_score_range and not popularity_range:
        query = build_query_with_director_name(director_name)
        res = es.search(index=INDEX_NAME, body=query)

    elif genre_term and not searchterm and not director_name \
            and not imdb_score_range and not popularity_range:
        query = build_query_with_genre(genre_term)
        res = es.search(index=INDEX_NAME, body=query)

    elif imdb_score_range and not searchterm and not director_name \
            and not genre_term and not popularity_range:
        query = build_query_with_imdb_score(imdb_score_lte, imdb_score_gte)
        res = es.search(index=INDEX_NAME, body=query)

    elif popularity_range and not searchterm and not \
            director_name and not genre_term and not \
            imdb_score_range:
        query = build_query_with_popularity(popularity_lte, popularity_gte)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and director_name and genre_term \
            and imdb_score_range and popularity_range:
        query = build_query_for_all(searchterm,
                                    genre_term, imdb_score_lte,
                                    imdb_score_gte, popularity_lte,
                                    popularity_gte, director_name)
        res = es.search(index="imdb-index", doc_type="imdb", body=query)

    elif searchterm and director_name and not genre_term \
            and not imdb_score_range and not popularity_range:
        query = build_query_for_searchterm_director(
            searchterm, director_name)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and imdb_score_range and not director_name \
            and not genre_term and not popularity_range:
        query = build_query_for_searchterm_imdb_score(
            searchterm, imdb_score_lte, imdb_score_gte)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and popularity_range and not director_name \
            and not genre_term and not imdb_score_range:

        query = build_query_for_searchterm_popularity(
            searchterm, popularity_lte, popularity_gte)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and genre_term and not director_name \
            and not imdb_score_range and not popularity_range:
        query = build_query_for_searchterm_genre(searchterm, genre_term)
        res = es.search(index=INDEX_NAME, body=query)

    elif director_name and imdb_score_range and not searchterm \
            and not popularity_range and not genre_term:
        query = build_query_for_director_name_imdb_score(
            imdb_score_lte, imdb_score_gte, director_name)
        res = es.search(index=INDEX_NAME, body=query)

    elif director_name and popularity_range and not searchterm \
            and not imdb_score_range and not genre_term:
        query = build_query_for_director_name_popularity(
            popularity_lte, popularity_gte, director_name)
        res = es.search(index=INDEX_NAME, body=query)

    elif director_name and genre_term and not popularity_range \
            and not imdb_score_range and not searchterm:
        query = build_query_for_director_name_genre(genre_term,
                                                    director_name)
        res = es.search(index=INDEX_NAME, body=query)

    elif imdb_score_range and popularity_range and not \
            director_name and not genre_term and not searchterm:
        query = build_query_for_imdb_score_popularity(
            imdb_score_lte, imdb_score_gte, popularity_lte, popularity_gte)
        res = es.search(index=INDEX_NAME, body=query)

    elif imdb_score_range and genre_term and not director_name \
            and not searchterm and not popularity_range:
        query = \
            build_query_for_imdb_score_genre(
                genre_term, imdb_score_lte, imdb_score_gte)
        res = es.search(index=INDEX_NAME, body=query)

    elif popularity_range and genre_term and not searchterm \
            and not director_name and not imdb_score_range:
        query = build_query_for_popularity_genre(genre_term,
                                                 popularity_lte,
                                                 popularity_gte)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and director_name and imdb_score_range \
            and not popularity_range and not genre_term:
        query = \
            build_query_for_searchterm_director_name_imdb_range(
                searchterm, director_name, imdb_score_lte,
                imdb_score_gte)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and imdb_score_range and popularity_range \
            and not director_name and not genre_term:
        query = \
            build_query_for_searchterm_imdb_range_popularity_range(
                searchterm, imdb_score_gte, imdb_score_lte,
                popularity_gte, popularity_lte)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and popularity_range and genre_term \
            and not director_name and not imdb_score_range:
        query = build_query_for_searchterm_popularity_range_genre(
            searchterm, popularity_gte, popularity_lte, genre_term)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and director_name and genre_term \
            and not imdb_score_range and not popularity_range:
        query = build_query_for_searchterm_director_name_genre(
            searchterm, director_name, genre_term)
        res = es.search(index=INDEX_NAME, body=query)

    elif director_name and imdb_score_range and popularity_range \
            and not searchterm and not genre_term:
        query = \
            build_query_for_director_name_popularity_range_imdb_score_range(
                director_name, popularity_lte, popularity_gte, imdb_score_gte,
                imdb_score_lte)
        res = es.search(index=INDEX_NAME, body=query)

    elif director_name and popularity_range and genre_term \
            and not searchterm and not imdb_score_range:
        query = \
            build_query_for_director_name_popularity_range_genre(
                director_name, popularity_lte, popularity_gte, genre_term)
        res = es.search(index=INDEX_NAME, body=query)

    elif imdb_score_range and popularity_range and genre_term \
            and not searchterm and not director_name:
        query = build_query_for_imdb_score_popularity_genre(
            genre_term, imdb_score_gte, imdb_score_lte,
            popularity_gte, popularity_lte)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and imdb_score_range and genre_term \
            and not director_name and not popularity_range:
        query = build_query_for_searchterm_imdb_score_genre(
            searchterm, imdb_score_lte, imdb_score_gte, genre_term)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and popularity_range and genre_term \
            and not director_name and not imdb_score_range:
        query = \
            build_query_for_searchterm_popularity_range_genre(
                searchterm, popularity_gte, popularity_lte, genre_term)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and director_name and popularity_range \
            and not imdb_score_range and not genre_term:
        query = \
            build_query_for_searchterm_director_name_popularity(
                searchterm, director_name, popularity_lte, popularity_gte)
        res = es.search(index=INDEX_NAME, body=query)

    elif director_name and imdb_score_range and genre_term \
            and not searchterm and not popularity_range:
        query = \
            build_query_for_director_name_imdb_score_range_genre(
                director_name, genre_term, imdb_score_gte, imdb_score_lte)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and director_name and imdb_score_range \
            and popularity_range and not genre_term:
        query = \
            build_query_for_searchterm_director_name_imdb_score_popularity_range(
                searchterm, director_name, imdb_score_gte,
                imdb_score_lte,
                popularity_lte, popularity_gte)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and imdb_score_range and popularity_range \
            and genre_term and not director_name:
        query = \
            build_query_for_searchterm_imdb_score_popularity_range_genre(
                searchterm, imdb_score_gte, imdb_score_lte,
                popularity_gte, popularity_lte, genre_term)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and director_name and popularity_range \
            and genre_term and not imdb_score_range:
        query = \
            build_query_for_searchterm_director_name_popularity_range_genre(
                searchterm, director_name, popularity_gte,
                popularity_lte,
                genre_term)
        res = es.search(index=INDEX_NAME, body=query)

    elif searchterm and director_name and imdb_score_range \
            and genre_term and not popularity_range:
        query = \
            build_query_for_searchterm_director_name_imdb_score_genre(
                searchterm, director_name, imdb_score_gte,
                imdb_score_lte, genre_term)
        res = es.search(index=INDEX_NAME, body=query)

    elif director_name and imdb_score_range and popularity_range \
            and genre_term and not searchterm:
        query = \
            build_query_for_director_name_imdb_score_popularity_range_genre(
                director_name, imdb_score_gte, imdb_score_lte, popularity_gte,
                popularity_lte, genre_term)
        res = es.search(index=INDEX_NAME, body=query)
    return error, res["hits"]["hits"]
