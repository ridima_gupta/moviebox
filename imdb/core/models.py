from django.contrib.auth.models import User
from django.db import models


class Imdb(models.Model):
    director_name = models.CharField(max_length=200)
    genre = models.CharField(max_length=400)
    imdb_score = models.DecimalField(max_digits=3,
                                     decimal_places=1,
                                     default=0.0)
    name = models.CharField(max_length=200)
    popularity = models.DecimalField(max_digits=3,
                                     decimal_places=1,
                                     default=0.0)

    def __unicode__(self):
        return self.name

    class Meta(object):
        unique_together = [("name", "director_name")]
        ordering = ['name']


class LastViewed(models.Model):
    imdb = models.ForeignKey(Imdb, default=None)
    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True, blank=True)
