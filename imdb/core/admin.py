# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from core.models import Imdb, LastViewed

admin.site.register(LastViewed)


@admin.register(Imdb)
class ImdbAdmin(admin.ModelAdmin):
    search_fields = ['director_name', 'genre', 'name']
    list_display = ('name', 'director_name', 'genre', 'imdb_score')
    list_filter = ('imdb_score',)
