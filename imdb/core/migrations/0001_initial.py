# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-08-05 05:35
from __future__ import unicode_literals

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Imdb',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('director_name', models.CharField(max_length=200)),
                ('genre', models.CharField(max_length=400)),
                ('imdb_score', models.DecimalField(decimal_places=1, default=0.0, max_digits=3)),
                ('name', models.CharField(max_length=200)),
                ('popularity', models.DecimalField(decimal_places=1, default=0.0, max_digits=3)),
            ],
        ),
        migrations.CreateModel(
            name='Recommendation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('imdb_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Imdb')),
                ('user_info',
                 models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='imdb',
            unique_together=set([('name', 'director_name')]),
        ),
    ]
