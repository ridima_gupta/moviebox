# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from core.models import Imdb, LastViewed
from search_and_filter_queries import fetch_recommendation, \
    search as search_in_es


# Create your views here.

def sign_up(request):
    if request.method == "GET":
        return render(request, 'register.html')

    # check if requisite data is present in post

    for key in ['email', 'password', 'username']:
        if key not in request.POST.keys():
            return render_to_response(request, 'register.html', {
                "status": "Make sure you fill all fields."})

    username = request.POST['username']
    email = request.POST['email']
    password = request.POST['password']

    # check if user exist
    if not (User.objects.filter(username=username).exists() or
            User.objects.filter(email=email).exists()):
        user = User.objects.create_user(username, email, password)
        user.save()
        return HttpResponseRedirect('/')
    else:
        return render(request,
                      'register.html',
                      {"status": "User with this "
                                 "email id and name exist"})


@require_http_methods(["GET", "POST"])
def user_login(request):
    if request.method == "GET":
        return render(request, "login.html")

    for key in ['username', 'password']:
        if key not in request.POST.keys():
            return render_to_response(request,
                                      'login.html',
                                      {"status": "Enter "
                                                 "Username "
                                                 "and Password"})

    username = request.POST['username']
    password = request.POST['password']

    user = authenticate(username=username,
                        password=password)
    if user is not None:
        if user.is_authenticated:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            return HttpResponse("This user is not active. "
                                "Please contact support@company.com")
    else:
        return HttpResponseRedirect('/login/')


@login_required(login_url='/login/')
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/login/')


@login_required(login_url='/login/')
def index(request):
    page = request.GET.get('page', 1)

    lastviewed, recommendation = None, None
    last_viewed_movie = LastViewed.objects.filter(
        user=request.user).order_by('-date')[:1]

    if last_viewed_movie:
        lastviewed = last_viewed_movie[0]
        if lastviewed.imdb.genre:
            res = fetch_recommendation(lastviewed.imdb.genre)

        if res.get('hits', {}).get('total', {}):
            value = res["hits"]["hits"]
            recommendation = value

    paginator = Paginator(Imdb.objects.all(), 25)
    try:
        obj_list = paginator.page(page)
    except PageNotAnInteger:
        obj_list = paginator.page(1)
    except EmptyPage:
        obj_list = paginator.page(paginator.num_pages)

    if lastviewed and recommendation:
        return render(request, 'list.html',
                      {'obj_list': obj_list,
                       'lastviewed': lastviewed,
                       'recommendation': recommendation})
    else:
        return render(request, 'list.html', {'obj_list': obj_list})


@login_required(login_url='/login/')
def view_record(request, url_id):
    imdb_obj = Imdb.objects.get(id=url_id)
    records = LastViewed(imdb=imdb_obj, user =request.user)
    records.save()
    return render(request, 'view.html', {'imdb_obj': imdb_obj})


# removing login_required so that postman calls
# don't need cookies or auth
# adding csrf_exempt
# @login_required(login_url='/login/')
@csrf_exempt
def search_json(request):
    """
    searchterm = "Star Wars"
    director_name = "Marc Daniels"
    genre_term = "Adventure, Sci-Fi"
    imdb_score_range = "4:9"
    popularity_range = "34:89"
    """
    searchterm = None
    director_name = None
    genre_term = None
    imdb_score_range = None
    popularity_range = None

    if request.method == "POST":
        if "searchterm" in request.POST:
            searchterm = request.POST['searchterm']
        if "director_name" in request.POST:
            director_name = request.POST['director_name']
        if "genre_term" in request.POST:
            genre_term = request.POST['genre_term']
        if "imdb_score_range" in request.POST:
            imdb_score_range = request.POST['imdb_score_range']
        if "popularity_range" in request.POST:
            popularity_range = request.POST['popularity_range']

        error, res = search_in_es(searchterm,
                                  director_name,
                                  genre_term,
                                  imdb_score_range,
                                  popularity_range)
        data_list = []
        if not error and res:
            for record in res:
                data_list.append(record["_source"])
            return render_to_response('results.html',
                                      {'data': data_list})
        else:
            return render_to_response('results.html')


@login_required(login_url='/login/')
@csrf_exempt
def search(request):
    """
    searchterm = "Star Wars"
    director_name = "Marc Daniels"
    genre_term = "Adventure, Sci-Fi"
    imdb_score_range = "4:9"
    popularity_range = "34:89"
    """
    searchterm = request.POST.get("text", "")
    filters = request.POST.get("filters", "")
    filter_list = None
    search_list = None

    if searchterm:
        search_list = searchterm.split(",")
    if filters:
        searchterm = None
        filter_list = filters.split(",")

    if search_list and filter_list:
        value_dict = dict(zip(filter_list, search_list))

    all_filters = {}

    for a_filter in filters.split(","):
        if a_filter == "Director":
            all_filters['director_name'] = value_dict["Director"].strip()
        elif a_filter == "Genre":
            all_filters['genre_term'] = value_dict["Genre"].strip()
        elif a_filter == "Score":
            all_filters['imdb_score_range'] = value_dict["Score"].strip()
        elif a_filter == "Popular":
            all_filters['popularity_range'] = value_dict["Popular"].strip()

    director_name = all_filters.get('director_name', None)
    genre_term = all_filters.get('genre_term', None)
    imdb_score_range = all_filters.get('imdb_score_range', None)
    popularity_range = all_filters.get('popularity_range', None)

    error, res = search_in_es(searchterm,
                              director_name,
                              genre_term,
                              imdb_score_range,
                              popularity_range)

    if error and not res:
        return HttpResponse("<p>No results</p>")

    return render_to_response('searchresults.html',
                              {'data': [record["_source"]
                                        for record in res
                                        if record.get("_source", False)]})
