import json
import os
import sys

import django
from elasticsearch import Elasticsearch

from core.models import Imdb

os.environ['DJANGO_SETTINGS_MODULE'] = 'imdb.settings'
django.setup()

COUNTER = 0
NO_OF_RECORDS = 0

ES_HOST = {"host": "localhost",
           "port": 9201}

es = Elasticsearch(hosts=[ES_HOST])

MAPPING = '''
{
  "mappings":{
    "imdb":{
      "properties":{
        "director_name":{
          "type":"text"
        },
        "imdb_score":{
          "type":"float"
        },
        "name":{
          "type":"text"
        },
        "genre":{
          "type":"text"
        },
        "popularity":{
          "type":"float"
        }
      }
    }
  }
}'''

INDEX_NAME = 'imdb-index'


def read_json_file(json_file_path):
    """
    The function is responsible for reading data from the json file
    :param json_file_path:
    the path from where one needs to read the json file containing data.
    :return: data read from file.
    """
    try:
        if os.path.isfile(json_file_path):
            with open(json_file_path) as json_data:
                data = json.load(json_data)
                json_data.close()
                return data
        else:
            print("File doesnt exist")
            print("total records to insert: " + str(NO_OF_RECORDS))
            print("records inserted: " + str(COUNTER))
            exit()
    except Exception as e:
        print(e.message)


def save_data(data):
    """
    This fucntion will help one save data in the DB.
    :param data: The data read from the json file
    :return:
    """
    global NO_OF_RECORDS
    NO_OF_RECORDS = len(data)
    global COUNTER
    payload = {}
    for record in data:
        director_name = record['director']
        genre_list = record['genre']
        genre = ','.join(genre_list)
        imdb_score = float(record['imdb_score'])
        name = record['name']
        popularity = float(record['99popularity'])

        payload["director_name"] = director_name
        payload["genre"] = genre
        payload["imdb_score"] = imdb_score
        payload["name"] = name
        payload["popularity"] = popularity

        records = Imdb(director_name=director_name,
                       genre=genre, imdb_score=imdb_score, name=name,
                       popularity=popularity)
        try:
            records.save()
            payload["db_id"] = records.id
            COUNTER = +COUNTER + 1
            es.index(index=INDEX_NAME, doc_type="imdb", body=payload)
        except Exception as e:
            print("Didnt not insert record with name: " +
                  name + " Error is: " + e.message)


if __name__ == "__main__":
    if len(sys.argv) == 3:
        json_file_path = sys.argv[1]
        if sys.argv[2]:
            create_new_index = sys.argv[2]
            if create_new_index == "True":
                if es.indices.exists(INDEX_NAME):
                    res = es.indices.delete(index=INDEX_NAME)
                res = es.indices.create(INDEX_NAME, body=MAPPING)
        data = read_json_file(json_file_path)
        save_data(data)
        print("total records to insert: " + str(NO_OF_RECORDS))
        print("records inserted: " + str(COUNTER))
    else:
        print("  Please provide all the inputs.")
        print(" \n Usage: If one needs to delete ES index "
              "\n Example: "
              "python save_data_to_DB_and_ES.py <path to json file> True "
              "\n \n Usage: "
              "If one doesn't want to delete ES index."
              "\n Example: "
              "python save_data_to_DB_and_ES.py <path to json file> False ")
